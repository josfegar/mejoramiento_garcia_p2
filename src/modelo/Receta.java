/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.TreeSet;

/**
 *
 * @author JOSE
 */
public class Receta {
private String nombre;
private int dificultad;
private int tiempo;
private String categoria;
private TreeSet<Ingrediente> ingredientes;
private String imagen;

    public Receta(String nombre, int dificultad, int tiempo, String categoria, TreeSet<Ingrediente> ingredientes, String imagen) {
        this.nombre = nombre;
        this.dificultad = dificultad;
        this.tiempo = tiempo;
        this.categoria = categoria;
        this.ingredientes = ingredientes;
        this.imagen = imagen;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getDificultad() {
        return dificultad;
    }

    public void setDificultad(int dificultad) {
        this.dificultad = dificultad;
    }

    public int getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public TreeSet<Ingrediente> getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(TreeSet<Ingrediente> ingredientes) {
        this.ingredientes = ingredientes;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
    
    public void agregarIngrediente(String nombre, String cantidad, double calorias){
        ingredientes.add(new Ingrediente(nombre, cantidad, calorias));
    }
    
    

}
