/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PaneOrganizer;

import java.io.File;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author JOSE
 */
public class VentanaPrincipalPane {
private BorderPane root; 
public VentanaPrincipalPane(){
    root= new BorderPane();
    root.setTop(cargarTop());
}
public VBox cargarTop(){
  File file=new File("src/recursos/portadatop.jpg");
  String ruta = null;
            try{
                ruta = file.toURI().toURL().toString();
            }catch(MalformedURLException e){
                e.getMessage();
            }
  Image image = new Image(ruta,1000,150,false,true);
  ImageView imageVw = new ImageView(image); 
  VBox top=new VBox();
  top.getChildren().addAll(imageVw);
  return top;
}

    public BorderPane getRoot() {
        return root;
    }

    public void setRoot(BorderPane root) {
        this.root = root;
    }

}
